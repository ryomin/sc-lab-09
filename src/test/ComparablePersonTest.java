package test;

import java.util.ArrayList;



import java.util.Collections;

import model.Person;

public class ComparablePersonTest {
	
	public static void main(String[] args) {
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("a", 0));
		persons.add(new Person("b", 500000));
		persons.add(new Person("c", 1000000));
		
		
		System.out.println("Before sort");
		for (Person p : persons) {
			System.out.println(p.getName());
		}
		
		Collections.sort(persons);
		System.out.println("\n After sort ");
		for (Person p : persons) {
			System.out.println(p.getName());
		}
		
	}


}

package test;

import java.util.ArrayList;
import java.util.Collections;

import model.Product;

public class ComparableProductTest {
	
	public static void main(String[] args) {
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("d", 10));
		products.add(new Product("e", 500));
		products.add(new Product("f", 1000000));

		
		System.out.println("Before sort");
		for (Product pro : products) {
			System.out.println(pro.getName());
		}
		
		Collections.sort(products);
		System.out.println("\n After sort");
		for (Product pro : products) {
			System.out.println(pro.getName());
		}
		
	}

}

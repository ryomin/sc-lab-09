package test;

import java.util.ArrayList;

import Interface.Taxable;
import model.Company;
import model.Person;
import model.Product;
import model.TaxCalculator;


public class TaxableTest {
	
	public static void main(String[] args){
		TaxableTest t = new TaxableTest();
		t.testTaxCalculator();
	}
	public void testTaxCalculator(){
		
		TaxCalculator t = new TaxCalculator();
		
		ArrayList<Taxable> p = new ArrayList<>();
		p.add(new Person("Naruto",500000));
		p.add(new Person("Sasuke", 100000));
		p.add(new Person("Sakura", 0));
		
		ArrayList<Taxable> c = new ArrayList<>();
		c.add(new Company("Jinhwan.J",400,300));
		c.add(new Company("Hanbin.H",500,500));
		c.add(new Company("Jewon.J",600,200));
		
		ArrayList<Taxable> pro = new ArrayList<>();
		pro.add(new Product("pen", 300));
		pro.add(new Product("pencil", 200));
		pro.add(new Product("liquid", 400));
		
		System.out.println("sum tax of person : "+t.sum(p)+" Bath.");
		System.out.println("sum tax of company : "+t.sum(c)+" Bath.");
		System.out.println("sum tax of product : "+t.sum(pro)+" Bath.");
		
		ArrayList<Taxable> allTax = new ArrayList<>();
		allTax.add(new Person("Minny",1000));
		allTax.add(new Company("Micky.M",400,300));
		allTax.add(new Product("Icecream", 300));
		System.out.println("sum all tax : "+t.sum(allTax)+" Bath.");
	}
	
	

	
	

}

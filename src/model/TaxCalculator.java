package model;

import java.util.ArrayList;

import Interface.Taxable;


public class TaxCalculator {
	
	public double sum(ArrayList<Taxable> taxList){
		double sum= 0;
		for(Taxable tax: taxList){
			sum += tax.getTax();
			
		}
		return sum;
	}

}

package model;


import Interface.Taxable;

public class Person implements Taxable, Comparable<Person> {
	
	private String name;
	private double high;
	private double salary;
	
	public Person(String aName, double aHigh){
		name = aName;
		high = aHigh;
		salary = aHigh;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHigh(){
		return high;
	}


	@Override
	public double getTax() {
		double n = 0;
		// TODO Auto-generated method stub
		if (salary <= 300000){
			return (int)(salary*(5/100));
		}
		n = salary - 300000;
		return (int)((300000*(5/100.0)) + (n*(10/100.0)));
	
	}

	@Override
	public int compareTo(Person other) {
		if (this.salary < other.salary ) { return -1; }
		if (this.salary > other.salary ) { return 1;  }
		return 0;
		// TODO Auto-generated method stub
	}

}

package model;

import Interface.Taxable;

public class Product implements Taxable, Comparable<Product> {
	
	private String name;
	private double price;
	
	public Product(String aName, double aPrice){
		name = aName;
		price = aPrice;
	}
	
	public String getName(){
		return name;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return (int)(price*(7/100.0));
	}

	@Override
	public int compareTo(Product other) {
		
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		// TODO Auto-generated method stub
		return 0;
	}

}
